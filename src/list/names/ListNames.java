package list.names;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;


public class ListNames {


    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите количество вводимых имен");
        int in = Integer.parseInt(reader.readLine());
        String[] str = new String[in];
        System.out.println("Введите пользователей, каждая запись заканчивается нажатием ввода");
        for (int i = 0; i < in; i++) {
            str[i] = reader.readLine();
        }
        ListNames names = new ListNames();
        names.SortNames(str);

    }
    private ArrayList<String> SortNames(String[] name){
        ArrayList<String> names = new ArrayList<>();
        for (String s : name) {
            names.add(s);
        }
        Collections.sort(names);
        System.out.println("Отсортированный список имен пользователей: ");
        for (String s : names) {
            System.out.println(s);
        }

        return names;
    }

}
